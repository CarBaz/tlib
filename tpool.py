#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
import multiprocessing as mp
import threading as th
import tspy

class Tpool_task(th.Thread):
    """Thread subclass for "Task" execution."""

    def __init__(self, target, args, kwargs, callback):
        """
        Return a new "Task".

        The thread is set as "Daemon" and heirs the name of the task function.
        """
        th.Thread.__init__(self)
        self.daemon    = True      # Set the thread as "Daemon".
        self._target   = target    # Target function to be executed.
        self._args     = args      # Arguments for target function.
        self._kwargs   = kwargs    # Keyword arguments for target function.
        self._callback = callback  # Callback function to execute over "Target" result.

        # Get the target function name for the thread name.
        self.name = self.name.replace('Thread',target.__name__)

    def run(self):
        """Run the target function and executes the callback function over the result."""
        self._callback(self._target(*self._args, **self._kwargs), self.name)

class Tpool_worker(mp.Process):
    """
    Process subclass for "Tasks" containment.

    Includes an internal "Tspy" for processes and threads monitoring.

    Includes its own workload montioring API.

    Method naming convention:
        Methods named normally are the ones to be called from parent process,
        "Manager", and so they are executed in parent's scope.

        Methods with mangled names (two leading underscores) inteds for internal
        process, "Worker", use only so they are executed in the self scope.
    """

    def __init__(self, name):
        """
        Return a new "Worker".

        The process is set as "Daemon" and it's renamed.
        """
        mp.Process.__init__(self)
        self.daemon   = True         # Set the process as "Daemon".
        self.name     = name         # Set the process name.
        self.tspy     = self         # Make the process its own "Tspy". This way the "get_report" method can be accessed.

        # Communication channel:
        pipe          = mp.Pipe()    # Communication Pipe.
        self._an_out  = pipe[0]      # Read end for answers.            Only the "Manager" process reads.
        self._an_in   = pipe[1]      # Write end for answers.           Only the "Worker" process writes.
        self._op_out  = pipe[1]      # Read end for operations.         Only the "Worker" process reads.
        self._op_in   = pipe[0]      # Write end for operations.        Only the "Manager" process writes.
        self._lock    = mp.Lock()    # Requests synchronization lock.
        self._enabled = True         # Flag the "Worker" as ready to receive requests.

    def run(self):
        """Execute the process's main loop."""
        self._tspy     = tspy.Tspy() # Create a real "Tspy" inside the process.
        self._loop     = True        # Main loop control flag.
        self._tasks    = {}          # "Tasks" list.
        self._workload = 0           # Sum of tasks workloads.

        # Main loop. It waits for operation requests.
        # Each request has an "Operation Code" and 5 arguments.
        # The arguments are named after the "new_task" interpretation for them.
        while self._loop:
            op, target, args, kwargs, workload, callback = self._op_out.recv()
            if   op == 'new_task':
                self.__new_task(target, args, kwargs, workload, callback)
            elif op == 'status':
                self.__get_status()
            elif op == 'report':
                self.__get_report()
            elif op == 'workload':
                self.__get_workload()
            elif op == 'kill':
                self._loop = False
        self.__clear_out(target, args)

    def new_task(self, target, args, kwargs, workload, callback):
        """Request a new "Task"."""
        with self._lock:
            if self._enabled:
                self._op_in.send(('new_task', target, args, kwargs, workload, callback))
                return True
            else:
                return False

    def get_status(self):
        """Request and return the status monitoring information."""
        with self._lock:
            if self._enabled:
                self._op_in.send(('status', None, None, None, None, None))
                return self._an_out.recv()
            else:
                return ('CLOSED {0}'.format(self.name), self._workload, [])

    def get_report(self):
        """Request and return the "Tspy" report."""
        with self._lock:
            if self._enabled:
                self._op_in.send(('report', None, None, None, None, None))
                return self._an_out.recv()
            else:
                return ((self.name, str(self), None), [], [])

    def get_workload(self):
        """Request and return the workload of active "Tasks"."""
        with self._lock:
            if self._enabled:
                self._op_in.send(('workload', None, None, None, None, None))
                return self._an_out.recv()
            else:
                return False

    def kill(self, wait, time):
        """Request to stop the "Worker"."""
        with self._lock:
            if self._enabled:
                self._op_in.send(('kill', wait, time, None, None, None))
                self._enabled = False
                return True
            else:
                return False

    def __new_task(self, target, args, kwargs, workload, callback):
        """Create, register and run the new "Task"."""
        task = Tpool_task(target, args, kwargs, self.__callback)
        self._tasks[task.name] = (task, workload, callback)
        self._workload += workload
        task.start()

    def __get_status(self):
        """Send the status monitoring information."""
        self._an_in.send((self.name, self._workload, self.__get_tasks()))

    def __get_report(self):
        """Send the "Tspy" report."""
        self._an_in.send(self._tspy.get_report())

    def __get_workload(self):
        """Send the workload of active "Tasks"."""
        self._an_in.send( (len(self._tasks), self._workload) )

    def __get_tasks(self):
        """Return the status monitoring information."""
        return [(task._target.__name__, task._args, task._kwargs, workload, callback.__name__)
                for task, workload, callback in sorted(self._tasks.values(), key=lambda x: x[0].name)]

    def __clear_out(self, wait, time):
        """Stop the "Worker" in an ordered way."""
        # Close the pipes. Note the "an" and "op" pipes are the same.
        self._an_in.close()
        self._an_out.close()

        # If requested join the "Tasks".
        if wait:
            for task, wl, cb in self._tasks.values():
                task.join(time)

    def __callback(self, result, task_name):
        """Pops the ending "Task" and propagates his result via the associated callback function."""
        task, workload, callback = self._tasks.pop(task_name)
        self._workload -= workload
        callback(result)

class Tpool_manager(mp.Process): #TODO: Add an option to purge empty workers avoiding loss of tasks.
    """
    Process subclass for "Workers" containment.

    Includes an internal "Tspy" for processes and threads monitoring.

    Includes its own workload montioring API.

    Method naming convention:
        Methods named normally are the ones to be called from calling processes,
        "Users", and so they are executed in parent's scope.

        Methods with mangled names (two leading underscores) inteds for internal
        process, "Manager", use only so they are executed in the self scope.
    """

    def __init__(self, name, policy):
        """
        Return a new "Manager".

        The process is set as "NOT Daemon" and it's renamed.
        """
        mp.Process.__init__(self)
        self.daemon    = False      # Set the process as NOT "Daemon". Daemonic processes can't have subprocesses.
        self.name      = name       # Set the process name.
        self.tspy      = self       # Make the process its own "Tspy". This way the "get_report" method can be accessed.
        self._policy   = policy     # "Task" distribution policy.

        # Communication channel:
        self._an_queue = mp.Queue() # Queue for returning answers.      Only the "Manager" process writes, any "Users" reads.
        self._op_queue = mp.Queue() # Queue for operation requests.     Only the "Manager" process reads, any "Users" writes.
        self._lock     = mp.Lock()  # Requests synchronization lock.
        self._enabled  = True       # Flag the "Worker" as ready to receive requests.

    def run(self):
        """Execute the process's main loop."""
        self._tspy    = tspy.Tspy() # Create a real "Tspy" inside the process.
        self._loop    = True        # Main loop control flag.
        self._workers = {}          # "Workers" list.

        # Main loop. It waits for operation requests.
        # Each request has an "Operation Code" and 5 arguments.
        # The arguments are named after the "new_task" interpretation for them.
        while self._loop:
            op, target, args, kwargs, workload, callback = self._op_queue.get()
            if   op == 'new_task':
                self.__new_task(target, args, kwargs, workload, callback)
            elif op == 'status':
                self.__get_status()
            elif op == 'report':
                self.__get_report()
            elif op == 'kill':
                self._loop = False
        self.__clear_out(target, args)

    def new_task(self, target, args, kwargs, workload, callback):
        """Request a new "Task"."""
        with self._lock:
            if self._enabled:
                self._op_queue.put(('new_task', target, args, kwargs, workload, callback))
                return True
            else:
                return False

    def get_status(self):
        """Request and return the status monitoring information."""
        with self._lock:
            if self._enabled:
                self._op_queue.put(('status', None, None, None, None, None))
                return self._an_queue.get()
            else:
                return ('CLOSED {0}'.format(self.name), [])

    def get_report(self):
        """Request and return the "Tspy" report."""
        with self._lock:
            if self._enabled:
                self._op_queue.put(('report', None, None, None, None, None))
                return self._an_queue.get()
            else:
                return ((self.name, str(self), None), [], [])

    def kill(self, wait, time):
        """Request to stop the "Manager"."""
        with self._lock:
            if self._enabled:
                self._op_queue.put(('kill', wait, time, None, None, None))
                self._enabled = False
                return True
            else:
                return False

    def __new_task(self, target, args, kwargs, workload, callback):
        """Choose a "Worker" and request to create a new "Task" in it."""
        worker = self._policy(self.__candidates())
        if worker:
            self._workers[worker].new_task(target, args, kwargs, workload, callback)
        else:
            self.__new_worker().new_task(target, args, kwargs, workload, callback)

    def __get_status(self):
        """Send the status monitoring information."""
        self._an_queue.put((self.name, self.__get_workers()))

    def __get_report(self):
        """Send the "Tspy" report."""
        self._an_queue.put(self._tspy.get_report())

    def __get_workers(self):
        """Return the status monitoring information."""
        return [worker.get_status() for worker in sorted(self._workers.values(),key=lambda x: x.name)]

    def __clear_out(self, wait, time):
        """Stop the "Manager" in an ordered way."""
        # Close the queues.
        self._an_queue.close()
        self._op_queue.close()

        # Send stop order to all "Workers".
        for worker in self._workers.values():
            worker.kill(wait, time)

        # Join the "Workers".
        for worker in self._workers.values():
            worker.join()

    def __new_worker(self):
        """Create, register and start a new "Worker"."""
        worker = Tpool_worker('Worker-{0}'.format(len(self._workers)))
        self._workers[worker.name] = worker
        worker.start()
        return worker

    def __candidates(self):
        """Return the list of "Worker" names and workloads sorted by task count."""
        # Request each "Worker" workload.
        candidates = [(worker.name, worker.get_workload()) for worker in self._workers.values()]

        # Order the list by task count.
        return sorted(candidates,key=lambda candidate: candidate[1][0])

### Predefined Policies:
def Policy_Direct_Spawn(candidates):
    """Cretate a new "Worker" for each "Task"."""
    return False

def Policy_CPU_Count_Limited_Spawn(candidates):
    """
    Create a new "Worker" for each "Task" until "cpu_count" "Workers".

    Once "cpu_count" is reached choose the one with less "Tasks"
    """
    w_count = len(candidates)
    if w_count < mp.cpu_count():
        worker = False
    else:
        # Assume "candidates" is sorted by task count.
        worker = candidates[0][0]
    return worker

def Policy_Task_Count_Balanced_Spawn(candidates):
    """
    Create a new "Worker" for each "Task" until "cpu_count"/2 "Workers".

    Once "cpu_count" is reached choose the one with less "Tasks" until
    each "Worker" have the same number of "Tasks" as "Workers".

    If the limit is reached for all "Workers" create a new one.
    """
    from math import ceil

    w_count = len(candidates)
    if w_count < ceil(mp.cpu_count()/2):
        worker = False
    else:
        # Assume "candidates" is sorted by task count.
        if candidates[0][1][0] < w_count:
            worker = candidates[0][0]
        else:
            worker = False
    return worker

def Policy_Workload_Balanced_10_Spawn(candidates):
    """
    Create a new "Worker" for each "Task" until "cpu_count"/2 "Workers".

    Once "cpu_count" is reached choose the one with less "Workload" until
    each "Worker" have a "Workload" of 10.

    If the limit is reached for all "Workers" create a new one.
    """
    from math import ceil

    w_count = len(candidates)
    if w_count < ceil(mp.cpu_count()/2):
        worker = False
    else:
        workload_sorted_candidates = sorted(candidates,key=lambda candidate: candidate[1][1])
        if workload_sorted_candidates[0][1][1] < 10:
            worker = workload_sorted_candidates[0][0]
        else:
            worker = False
    return worker

DefaultP = Policy_Workload_Balanced_10_Spawn

# Note for new policies implementation:
#   "candidates" :: [(Worker.name, (Size, Workload))] sorted by ascending "Size".

### End of Predefined Policies.

def Foonction_Callbar(foo):
    """Null function to be used as a "Null Callback"."""
    pass

class Tpool:
    """Facade for "Tpool_manager" access and proxy creation."""

    def __init__(self, name, policy=DefaultP):
        """Return a "Tpool" with a "Manager" inside."""
        self.manager = Tpool_manager(name, policy)

    def start(self):
        """Start the "Manager"."""
        self.manager.start()

    def get_name(self):
        """Request "Manager" name."""
        return self.manager.name

    def new_task(self, target, args=(), kwargs={}, workload=1, callback=Foonction_Callbar):
        """Request a new "Task"."""
        return self.manager.new_task(target, args, kwargs, workload, callback)

    def get_status(self):
        """Request and return the status monitoring information."""
        return self.manager.get_status()

    def kill(self, wait=False, time=0):
        """Request to stop the "Manager"."""
        return self.manager.kill(wait, time)

    def join(self):
        """Join the "Manager"."""
        self.manager.join()

def Launch(name, policy=DefaultP):
    """Create a "Tpool" running."""
    pool = Tpool(name, policy)
    pool.start()
    return pool
