Tlib.
Copyright (c) 2014, Carlos Bazaga.
Licensed under BSD 2-Clause.
All rights reserved.

A simple helping library for python multiprocessing.

Actually includes the following modules:

################################# Done or almost done. #####################################

tspy.py             :   Tspy objects are itended to be created inside a process
                        to get information about his threads and subprocesses.

tpool.py            :   Tpool objects are a process derived class that manages a pool
                        of Workers. Each Tpool Worker is a container process derived
                        class for Task threads. Task are requestd to the Tpool and it
                        distributes its load between Workers.
                        Each worker spawns thread for each Task.

                        The policy for load distribution can be given as an argument.

                        We get a pool of worker processes wich is also multithreaded and
                        with a programmable policy.
                        This enables to an easy taks load distribution.

tmanager.py         :   This class defines a derived class of SyncManager with some TLib
                        classes preregistered and an internal "Tspy" object instanced.
                        This enables the use of proxies, even Tpool proxies, and deep
                        tracing of spawned processes and threads.

tmonitor.py         :   Tmonitor is a Tk App for Tspy (Subprocesses and threads)
                        and Tpools monitorization.

                        Tpools can be added or removed "on the fly"
                        It also porvides mechanisms for "on the fly" button spawn associted
                        to given functions.

                        Also offers new windows creation with "write" methods to be used
                        as "output" for any task, thread or process.
                        Those windows "Terminals" can also host buttons as the main window.

tprocessing.py      :   Implements Tprocess, an derived class for "multiprocessing.Process"
                        wich adds a monitorization mechanism via "Tspy".
                        It can be imported in replace of "multiprocessing" in any case.
                        Tprocesses can be used almost always in replacement of "process"
                        and even extended by user classes just like "process".

tzrpc_server.py     :   Service Tprocess for a "ZeroRPC" server.
                        (https://github.com/dotcloud/zerorpc-python)

                        Tzrpc_server a zerorpc server in its own Tprocess derived class.
                        ZeroRPC and its dependencias must be installed separately.
                        It works well with a 'proxied' Tpool to expose Tpool
                        functionallity to other processes, or machines via network.

                        Warning, be carefull about monkeypatching modules, "threading",
                        for example, can cause the Tmanager and Tpool modules to fail.


#################################   Fully to be done.  #####################################

tsingles.py         :   Pretend to implement a "singleton" pattern based in
                        "unique module instance" and Tmanagers.


#################################   Demos and tests.   #####################################

Demo_1.py           :   Actual working Demo wit a weird Tmanager/Tpool/Tprocesses tree, a
                        Tzerorpc_server and a Tmonitor to demostrates Tlib capabilities.


#################################   Work in progres.   #####################################

Demo_2.py           :   A new Demo with a more common Tmanager/Tpool/Tprocesses tree.


