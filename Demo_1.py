#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from __future__ import print_function
import tprocessing as tp
import threading as th
import tzrpc_server
import time

#TODO: Clean, order and add some documentation.
#TODO: Migrate some relevant code.
#TODO: Better documentation.

class Api:
    """Tpool" access Api."""

    def __init__(self, name, manager):
        """Return a "Tpool" access Api object."""
        self.name    = name
        self.manager = manager
        self.valor   = 0

    def get_name(self):
        """Return service name."""
        return self.name

    def get_valor(self):
        """Return service value."""
        return self.valor

    def get_time(self):
        """Return service time."""
        return time.asctime()

    def set_valor(self, val):
        """Set service value."""
        self.valor = val
        return True

    def loop_task(self, text, interval, iterations):
        """Create a loop "Task"."""
        return self.manager.new_task(loop_task, args=(str(text), interval, iterations), callback=print)

    def single_task(self, text, interval):
        """Create a single "Task"."""
        return self.manager.new_task(single_task, args=(str(text), interval), callback=print)

class Api_bridge:
    """"
    Api" bridge connector to the RPC service.

    Comments are writed in order to be readable through Zero-RPC commandline.
    """

    def __init__(self, name, api):
        """Return an Api_bridge object."""
        self.name    = name
        self.api     = api

    def get_name(self):
        """()                                     -> Return service name."""
        return self.api.get_name()

    def get_valor(self):
        """()                                     -> Return service value."""
        return self.api.get_valor()

    def get_time(self):
        """()                                     -> Return service time."""
        return self.api.get_time()

    def set_valor(self, val):
        """(value)                                -> Set service value."""
        return self.api.set_valor(val)

    def loop_task(self, text, interval, iterations):
        """(text, interval, iterations)           -> Create a loop "Task"."""
        return self.api.loop_task(text, float(interval), int(iterations))

    def single_task(self, text, interval):
        """(text, interval)                       -> Create a single "Task"."""
        return self.api.single_task(text, float(interval))

    def bulk_loop_task(self, text, interval, iterations, quantity):
        """(text, interval, iterations, quantity) -> Create "quantity" single "Tasks"."""
        for i in range(int(quantity)):
            self.api.loop_task(text, float(interval), int(iterations))
        return True

    def bulk_single_task(self, text, interval, quantity):
        """(text, interval, quantity)             -> Create "quantity" loop "Tasks"."""
        for i in range(int(quantity)):
            self.api.single_task(text, float(interval))
        return True

    def slowmethod(self):
        """()                                     -> A slow executing method returning 'Slow Done' string."""
        for x in range(10000):
            if x%100 == 0:
                time.sleep(0.00001)
            for n in range(10000):
                y = x*x
        return 'Slow Done'

    def fastmethod(self):
        """()                                     -> A fast executing method returning 'Fast Done' string."""
        return 'Fast Done'

# Functions to test Tmonitor terminals.
def null_task():
    """Do nothing."""
    pass

def timestamp(target):
    """Write a timestamp on the target."""
    target.write('{0}\n'.format(time.asctime()))

def spam(text, target):
    """Spams "taget" with write(text)."""
    ready = True
    while ready:
        try:
            target.write(text)
        except Exception as exc:
            print('{0} Exits cause write error: {1}'.format(th.current_thread().name, exc))
            ready = False
        else:
            time.sleep(2)

def launch_spam(target, statics={'i':0}):
    """Launch a thread with "spam" as target.
    Each call increments the name of the thread by one.
    """
    name = 'Spammer {0}'.format(statics['i'])
    Spammer = th.Thread(target=spam, args=('SPAM!!! {0}\n'.format(name), target), name=name)
    Spammer.daemon = True
    Spammer.start()
    statics['i'] += 1
    return Spammer

# Functions to test Tmonitor.
def button_later(Mon, Time):
    """
    Create a new button on "Mon" Tmonitor after "time" seconds.
        This button will create a new Tmonitor terminal window.
        On close that window will "bell".
    """
    time.sleep(Time)
    Mon.spawn_buttons([[('New Terminal', Mon.pop_terminal, ('Terminal',), {'close_callback':Mon.bell})]])

def get_Monitor(title, tpools, interval, S_Wait, L_Wait, L_Loop, Bulk):
    """Create and configure a monitoring window."""
    return tp.Tmonitor(title, interval, tpools,
                           [ [('Tpool-0 Single',                    launch_task,      (tpools[0], single_task, ('Soy una tarea simple.', S_Wait),        print),{})
                             ,('Tpool-0 Loop',                      launch_task,      (tpools[0], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print),{})
                             ,('Tpool-0 Single x {0}'.format(Bulk), launch_bulk_task, (tpools[0], single_task, ('Soy una tarea simple.', S_Wait),        print, Bulk),{})
                             ,('Tpool-0 Loop x {0}'.format(Bulk),   launch_bulk_task, (tpools[0], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print, Bulk),{})]
                            ,[('Tpool-1 Single',                    launch_task,      (tpools[1], single_task, ('Soy una tarea simple.', S_Wait),        print),{})
                             ,('Tpool-1 Loop',                      launch_task,      (tpools[1], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print),{})
                             ,('Tpool-1 Single x {0}'.format(Bulk), launch_bulk_task, (tpools[1], single_task, ('Soy una tarea simple.', S_Wait),        print, Bulk),{})
                             ,('Tpool-1 Loop x {0}'.format(Bulk),   launch_bulk_task, (tpools[1], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print, Bulk),{})]
                            ,[('Tpool-2 Single',                    launch_task,      (tpools[2], single_task, ('Soy una tarea simple.', S_Wait),        print),{})
                             ,('Tpool-2 Loop',                      launch_task,      (tpools[2], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print),{})
                             ,('Tpool-2 Single x {0}'.format(Bulk), launch_bulk_task, (tpools[2], single_task, ('Soy una tarea simple.', S_Wait),        print, Bulk),{})
                             ,('Tpool-2 Loop x {0}'.format(Bulk),   launch_bulk_task, (tpools[2], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print, Bulk),{})]
                            ,[('Tpool-3 Single',                    launch_task,      (tpools[3], single_task, ('Soy una tarea simple.', S_Wait),        print),{})
                             ,('Tpool-3 Loop',                      launch_task,      (tpools[3], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print),{})
                             ,('Tpool-3 Single x {0}'.format(Bulk), launch_bulk_task, (tpools[3], single_task, ('Soy una tarea simple.', S_Wait),        print, Bulk),{})
                             ,('Tpool-3 Loop x {0}'.format(Bulk),   launch_bulk_task, (tpools[3], loop_task,   ('Soy una tarea bucle.', L_Wait, L_Loop), print, Bulk),{})]
                            ])

# Functions to test Tpool.
def loop_task(text, interval, iterations, k=0):
    """Echo "text" each "interval" seconds for "iterations" times."""
    i=0
    while i < iterations:
        print('{0:24} {1:30} (LOOP) {2}'.format('{0}.{1}:'.format(tp.current_process().name,th.current_thread().name),text,k))
        time.sleep(interval)
        i += 1
    return '{0:24} {1:30} (CB) {2}'.format('{0}.{1}:'.format(tp.current_process().name,th.current_thread().name),text,k)

def single_task(text, interval, k=0):
    """Echo "text", sleep "interval" seconds and echo "text" again."""
    print('{0:24} {1:30} (IN) {2}'.format('{0}.{1}:'.format(tp.current_process().name,th.current_thread().name),text,k))
    time.sleep(interval)
    print('{0:24} {1:30} (OUT) {2}'.format('{0}.{1}:'.format(tp.current_process().name,th.current_thread().name),text,k))
    return '{0:24} {1:30} (CB) {2}'.format('{0}.{1}:'.format(tp.current_process().name,th.current_thread().name),text,k)

def launch_task(Tpool, task, args, cb):
    """Create a "Task"."""
    Tpool.new_task(task, args=args, kwargs={'k':123}, callback=cb)

def launch_bulk_task(Tpool, task, args, cb, ite):
    """Create "quantity" "Tasks"."""
    for i in range(ite):
        Tpool.new_task(task, args=args, kwargs={'k':123}, workload=2, callback=cb)

def MAIN():
    """Main program code."""
    # Define some constants.
    Protocol = "tcp"
    Address  = "0.0.0.0"
    Port     = "4242"

    Mon_Time = 1
    S_Wait   = 60
    L_Wait   = 2
    L_Loop   = 10
    Bulk     = 13

    # Register classes on "Tmanager".
    tp.Tmanager.register('Api',Api)

    # Create a "Tmanager" for proxies.
    MyManager1 = tp.Tmanager('MyManager1')
    MyManager1.start()
    print('Registered classes on "Tmanager".')

    # Create a second "Tmanager".
    MyManager2 = tp.Tmanager('MyManager2')
    MyManager2.start()
    print('Created a second "Tmanager".')

    # Create a "Tmanager" inside "MyManager2".
    MyManager3 = MyManager2.Tmanager('MyManager3')
    MyManager3.start()
    print('Created a "Tmanager" inside "MyManager2".')

    # Create a "Tmanager" inside "MyManager3".
    MyManager4 = MyManager3.Tmanager('MyManager4')
    MyManager4.start()
    print('Create a "Tmanager" inside "MyManager3".')

    # Create an unshared "Tpool".
    MyPool0 = tp.Tpool('Tpool-0 "Not_shared"')
    MyPool0.start()
    print('Create an unshared "Tpool".')

    # Create a "Tpool" proxy.
    # If no proxy used it will except when queue is handled.
    MyPool1 = MyManager1.Tpool('Tpool-1')
    MyPool1.start()
    print('Create a "Tpool" proxy.')

    # Create a second "Tpool" proxy.
    MyPool2 = MyManager2.Tpool('Tpool-2')
    MyPool2.start()
    print('Create a second "Tpool" proxy.')

    # Create a third "Tpool" proxy.
    # It's housed in a "Tmanager" inside another "Tmanager".
    MyPool3 = MyManager4.Tpool('Tpool-3')
    MyPool3.start()
    print('Create a third "Tpool" proxy.')

    # Create an "Api" proxy.
    # Unnecessary unless "Api" sharing.
    # It will be shared with "Zserver".
    MyApi  = MyManager1.Api('Api-1', MyPool3)
    print('Create an "Api" proxy.')

    # Create a "bridge" to filter "Api" access from "Zserver".
    # Maybe a good idea to be shared, mainly if it have self values.
    MyBridge = Api_bridge("Bridge-1", MyApi)
    print('Create a "bridge" to filter "Api" access from "Zserver".')

    # Create and start a "Zserver" with an "Api" connected through the "Bridge".
    MyServer = tzrpc_server.Tzrpc_server("Zserver-1", Protocol, Address, Port, MyBridge)
    MyServer.start()
    print('Create and start a "Zserver" with an "Api" connected through the "Bridge".')

    # Create a "Monitor".
    MyMon = get_Monitor("Tmonitor-1", [MyPool0, MyPool1, MyPool2, MyPool3], Mon_Time, S_Wait, L_Wait, L_Loop, Bulk)

    # Create a terminal window in the "Monitor".
    MyTerm = MyMon.pop_terminal('Terminal 0')

    # Create buttons on the terminal window.
    MyTerm.spawn_buttons([[ ('Write Time', timestamp,    (MyTerm,), {}),
                            ('Start Spam', launch_spam,  (MyTerm,), {}),
                            ('Clear Term', MyTerm.clear, (),        {})]])

    # TODO: Need to test Task inside Tpools (top level and deeper) writting in terminals.

    # Program a new button spawn within ten seconds.
    MyLater = th.Thread(target=button_later, args=(MyMon, 10), name='Button Spawn')
    MyLater.daemon = True
    MyLater.start()

    # Launch monitor.
    MyMon.start()
    print('BYE MyMon')

    # Launch monitor in an indeendent thread to avoid main thread blocking.
    # th.Thread(target=MyMon.start, name='Tmonitor').start()
    # print('Non blocking Monitor.')
    # time.sleep(10)
    # print('Closing services.')

    # MyTerm2 = MyMon.pop_terminal('Spammer1')
    # time.sleep(5)
    # try:
        # MyTerm2.close()
    # except Exception as err:
        # print('\nThis is the end: {0}\n'.format(err))
    # else:
        # print('\nThis is the end: No Except\n')

    # Stop "Zserver".
    MyServer.stop()

    # Wit until "Zserver" ends.
    MyServer.join()
    print('BYE MyServer')

    # Stop "Tpools".
    MyPool0.kill()
    MyPool1.kill()
    MyPool2.kill()
    MyPool3.kill()

    # Wait until "Tpools" ends.
    MyPool0.join()
    MyPool1.join()
    MyPool2.join()
    MyPool3.join()
    print('BYE MyPools')

    # Stop "Tmanagers". starting from deeper one.
    MyManager4.shutdown()
    MyManager3.shutdown()
    MyManager1.shutdown()
    MyManager2.shutdown()

def END():
    """Code before program exit."""
    raw_input('\nPress enter to exit...')
    pass

if __name__ == '__main__':
    tp.freeze_support()
    MAIN()
    END()
