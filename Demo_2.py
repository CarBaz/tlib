#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from __future__ import print_function
import tprocessing as mp
import threading as tp
import tzrpc_server
import time

'''
    We are going to build a multiprocess structure
    It wil consist in:
    Some services runing in its own process.
        A ZeroRPC server to allow system remote control.
    Some Tpools to distribute workload.
    A structure of managers to manage accesto to a shared data structure.
        The data estructure will be a ojects hierachy.
            Root class.
                2 Trees of data container objects.
                    Data objects.


    A monitor of the structure to visulize its behavior.
'''

class Api:# Done.
    """Define an API for Data Structure operations."""
    def __init__(self, root):
        self.root = root

    def new_tree(self, name):
        self.root.new_tree(name)

    def new_head(self, tree, name):
        self.root.new_head(tree, name)

    def new_holder(self, head, name):
        self.root.new_holder(head, name)

    def set_child(self, head, parent, child):
        self.root.set_child(head, parent, child)

    def new_memo(self, head, holder, name, value):
        self.root.new_memo(head, holder, name, value)

    def new_data(self, head, holder, name, value, step):
        self.root.new_data(head, holder, name, value, step)

    def set_memo(self, head, holder, name, value):
        self.root.set_memo(head, holder, name, value)

    def set_data(self, head, holder, name, value):
        self.root.set_data(head, holder, name, value)

    def view_memo(self, head, holder, name):
        self.root.view_memo(head, holder, name)

    def view_data(self, head, holder, name):
        self.root.view_data(head, holder, name)

class Root:# Done.
    """Define an access point for the data trees."""
    def __init__(self, name):
        self.name   = name
        self.trees  = {}
        self.heads  = {}
        self.single = None

    def set_single(self, single):
        self.single = single

    def new_tree(self, name):
        self.trees[name] = mp.Tmanager('{0}-Tmanager'.format(name))
        self.trees[name].start()

    def new_head(self, tree, name):
        self.heads[name] = self.trees[tree].Head(name, self.single)

    def new_task(self, head, target, args=(), kwargs={}, workload=1, callback=None):
        self.heads[head].tpool.new_task(target, args=(), kwargs={}, workload=1, callback=None)
## Uplinked

    def new_holder(self, head, name):
        self.heads[head].new_holder(name)

    def set_child(self, head, parent, child):
        self.heads[head].set_child(parent, child)

    def new_memo(self, head, holder, name, value):
        self.heads[head].new_memo(holder, name, value)

    def new_data(self, head, holder, name, value, step):
        self.heads[head].new_data(holder, name, value, step)

    def set_memo(self, head, holder, name, value):
        self.heads[head].set_memo(holder, name, value)

    def set_data(self, head, holder, name, value):
        self.heads[head].set_data(holder, name, value)

    def view_memo(self, head, holder, name):
        return self.heads[head].view_memo(holder, name)

    def view_data(self, head, holder, name):
        return self.heads[head].view_data(holder, name)

class Head:# Done.
    """Define an access point for the data trees."""
    def __init__(self, name, single):
        self.name    = name
        self.holders = {}
        self.tpool   = mp.Tpool('{0}-Tpool'.format(name))
        self.tpool.start()

        self.single  = single

    def new_holder(self, name):
        self.holders[name] = Holder(name, self)

## Uplinked

    def set_child(self, parent, child):
        self.holders[parent].set_child(self.holders[child])

    def new_memo(self, holder, name, value):
        self.holders[holder].new_memo(name, value)

    def new_data(self, holder, name, value, step):
        self.holders[holder].new_data(name, value, step)

    def set_memo(self, holder, name, value):
        self.holders[holder].set_memo(name, value)

    def set_data(self, holder, name, value):
        self.holders[holder].set_data(name, value)

    def view_memo(self, holder, name):
        return self.holders[holder].view_memo(name)

    def view_data(self, holder, name):
        return self.holders[holder].view_data(name)

class Holder:# Done.
    """Define an access point for the data trees."""
    def __init__(self, name, head):
        self.name    = name
        self.head    = head
        self.datas   = {}
        self.memos   = {}
        self.childs  = {}
        self.updater = Updater(self, 5)
        self.updater.start()

    def set_child(self, child):
        self.childs[name] = child

    def new_memo(self, name, value):
        self.memos[name] = Memo(name, value)

    def new_data(self, name, value, step):
        self.datas[name] = Data(name, value, step, self.head, self)

## Uplinked

    def set_memo(self, name, value):
        self.memos[name].set(value)

    def set_data(self, name, value):
        self.datas[name].set(value)

    def view_memo(self, name):
        return self.memos[name].view()

    def view_data(self, name):
        return self.datas[name].view()

    def update(self):
        for data in self.datas.values():
            data.update()

class Memo:# Done.
    """A data item with a "set" update function."""
    def __init__(self, name, value):
        self.name  = name
        self.value = value

    def set(self, value):
        self.value = value

    def view(self):
        return self.value

class Data:# Done.
    """A data item with an auto update function."""
    def __init__(self, name, value, step, head, holder):
        self.name   = name
        self.head   = head
        self.holder = holder
        self.value  = value
        self.step   = step

    def set(self, value):
        self.value = value

    def update(self):

        def receive2(data, head, holder, name): #, single):
            value, step = data
            result = value + step
            print(result)
            # single['root'].set_data(head, holder, name, result)
            print('BYE')

        def ridiculous():
            print('Ridiculous')

        self.head.tpool.new_task(receive, args=((self.value, self.step), self.head.name, self.holder.name, self.name, self.head.single))
        # self.head.tpool.new_task(receive2, args=((self.value, self.step), self.head.name, self.holder.name, self.name))#, self.head.single))
        self.head.tpool.new_task(ridiculous)

    def view(self):
        return self.value

class Updater(tp.Thread):# Done.
    def __init__(self, target, interv):
        tp.Thread.__init__(self)
        self.name   = '{0}-Updater'.format(target.name)
        self.interv = interv
        self.target = target
        self.daemon = True

    def run(self):
        it = 0
        while it < 20:
            self.target.update()
            time.sleep(self.interv)
            it += 1

class RPC_Api_Bridge:# Done.
    """Define a set of operation to be offered via the RPC server."""
    def __init__(self, api):
        """."""
        self.api = api

    def new_tree(self, name):
        """(name)"""
        self.api.new_tree(name)

    def new_head(self, tree, name):
        """(tree, name)"""
        self.api.new_head(tree, name)

    def new_holder(self, head, name):
        """(head, name)"""
        self.api.new_holder(head, name)

    def set_child(self, head, parent, child):
        """(head, parent, child)"""
        self.api.set_child(head, parent, child)

    def new_memo(self, head, holder, name, value):
        """(head, holder, name, value)"""
        self.api.new_memo(head, holder, name, int(value))

    def new_data(self, head, holder, name, value, step):
        """(head, holder, name, value, step)"""
        self.api.new_data(head, holder, name, int(value), int(step))

    def set_memo(self, head, holder, name, value):
        """(head, holder, name, value)"""
        self.api.set_memo(head, holder, name, int(value))

    def set_data(self, head, holder, name, value):
        """(head, holder, name, value)"""
        self.api.set_data(head, holder, name, int(value))

    def view_memo(self, head, holder, name):
        """(head, holder, name)"""
        self.api.view_memo(head, holder, name)

    def view_data(self, head, holder, name):
        """(head, holder, name)"""
        self.api.view_data(head, holder, name)

def receive(data, head, holder, name, single):
    value, step = data
    result = value + step
    print(result)
    single['root'].set_data(head, holder, name, result)
    print('BYE')

# Register new classes in the Tmanager
mp.Tmanager.register('Api', Api)
mp.Tmanager.register('Root', Root)
mp.Tmanager.register('Head', Head)

def MAIN():
    """Main source."""
    # RPC_Server connection data.
    Protocol = "tcp"
    Address  = "0.0.0.0"
    Port     = "4242"

    # Create an empty Tmonitor.
    MyMonitor = mp.Tmonitor("Demo Monitor", 1, [], [])

    # Create Main Tmanager.
    MyManager = mp.Tmanager('Main-Tmanager')
    MyManager.start()

    # Create Second Tmanager.
    MyManager2 = mp.Tmanager('Second-Tmanager')
    MyManager2.start()

    # Create Root.
    MyRoot = MyManager.Root('Main-Root')

    single = MyManager2.dict()

    single['root'] = MyRoot

    MyRoot.set_single(single)

    # Create Api.
    MyApi = MyManager2.Api(MyRoot)

    # Create an acces bridge for API.
    MyApiBridge = RPC_Api_Bridge(MyApi)

    # Create a RPC server.
    MyServer = tzrpc_server.Tzrpc_server("Demo Zserver", Protocol, Address, Port, MyApiBridge)
    MyServer.start()

    # Launch Monitor.
    MyMonitor.start()

    # Stop server.
    # MyServer.stop()
    # MyManager.shutdown()
    # MyManager2.shutdown()

def END():
    """End source."""
    # raw_input('\nPress any key to exit...')
    pass

if __name__ == '__main__':
    mp.freeze_support()
    MAIN()
    END()
