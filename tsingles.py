# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from multiprocessing.managers import SyncManager, BaseManager
import multiprocessing as mp
import gestor_protocolos as GP
import gestor_layouts as GL
import dispatcher

class MyManager(SyncManager):
    """This class defines a subuclass of SyncManager."""
    pass

# Class registration on managers.
MyManager.register('disp',dispatcher.Dispatcher)
MyManager.register('ges_pro',GP.Gestor_protocolos)
MyManager.register('ges_lays',GL.Gestor_layouts)

# Managers declaration. A process for eac manager.
dispatcher_manager = MyManager()
dispatcher_manager.start()

protocols_manager = MyManager()
protocols_manager.start()

lays_manager = MyManager()
lays_manager.start()

# Singletons instantiation.
disp     = dispatcher_manager.disp()
ges_pro  = protocols_manager.ges_pro()
ges_lays = lays_manager.ges_lays()
