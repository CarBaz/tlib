#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from multiprocessing.managers import SyncManager
from tpool import Tpool
from tspy import Tspy

class Tmanager(SyncManager):
    """
    This class defines a subuclass of SyncManager with some TLib classes preregistered and
    an internal "Tspy" object instanced.

    The classes preregistered for public use are:
        Tmanager as "Tmanager"
        Tpool as "Tpool"

    Aditionally an "only for internal use" registering is made:
        Tspy as "A_Tspy_Inside"

    The internal Tspy's methods are reachable from outside via the proxy mechanism
    handled by the parent class SyncManager.
    """

    def __init__(self, name):
        """
        Return a new Tmanager.

        Tmanagers process's name will be overwritten with "name".
        """
        SyncManager.__init__(self)
        self.name = name

    def start(self):
        """
        Spawn a server process for this manager object and create a Tspy object inside.

        The Tspy will be accesible as a proxy object on Tmanager's "tspy" atribute.
        """
        SyncManager.start(self)
        self.tspy = self.A_Tspy_Inside(self.name)

        # Now we add the Tspy proxy to the Tmanager's process and edit his name.
        self._process.tspy = self.tspy
        self._process.name = self.name

    def shutdown(self):
        """
        Bridge method to parent class's 'shutdown' method.

        This bridge allows Tmanagers inside a Tmanager to be stopped.

        Tmanagers inside a Tmanager should be shutted down from innermost to outermost.
        """
        SyncManager.shutdown(self)

    def join(self):
        """
        Bridge method to parent class's 'join' method.

        This bridge allows Tmanagers inside a Tmanager' process to be joined.

        Tmanagers inside a Tmanager should be joined from innermost to outermost.
        """
        SyncManager.join(self)

### Registration of default Tlib classes on Tmanager.
Tmanager.register('A_Tspy_Inside', Tspy)
Tmanager.register('Tmanager', Tmanager)
Tmanager.register('Tpool', Tpool)

def Launch(name):
    manager = Tmanager(name)
    manager.start()
    return manager
